/* global __DEV__ */
import React, { useEffect, useRef, useState } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { AppContext } from './shared/context'
import deviceStorage from './shared/deviceStorage'
import { Splash } from './screens/Splash'
import { LoginManager } from 'react-native-fbsdk'
import { GoogleSignin } from '@react-native-community/google-signin'
import axios from 'axios'
import reactotron from 'reactotron-react-native'
import { RootStackScreen } from './navigation'
import io from 'socket.io-client'
import PushNotification from 'react-native-push-notification'

import { YellowBox } from 'react-native'

YellowBox.ignoreWarnings([
  'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, ' +
  '`pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. ' +
  'Did you mean to put these under `headers`?'
])

if (__DEV__) {
  import('./ReactotronConfig').then(() => console.log('Reactotron Configured'))
}

PushNotification.configure({
  onRegister: function (token) {
    console.log('TOKEN:', token)
  },

  onNotification: function (notification) {
    console.log('NOTIFICATION:', notification)
  },

  permissions: {
    alert: true,
    badge: true,
    sound: true
  },

  popInitialNotification: true,
  requestPermissions: false
})

function notify (title, message) {
  PushNotification.localNotification({
    title: title,
    message: message
  })
}

export default function App () {
  const [isLoading, setIsLoading] = React.useState(false)
  const [location, setLocation] = useState({})
  const [user, setUser] = useState({})
  const [token, setToken] = useState(null)
  const [firstUse, setFirstUse] = useState('true')
  const [chats, setChats] = useState([])
  const socket = useRef(null)

  useEffect(() => {
    const loadAppState = async () => {
      await deviceStorage.loadToken(setToken)
      await deviceStorage.loadFirstUse(setFirstUse)
      await deviceStorage.loadUser(setUser)
      await deviceStorage.loadLocation(setLocation)
    }
    loadAppState()
  }, [])

  // load chats if user is logged in
  useEffect(() => {
    async function load () {
      const storedChats = await deviceStorage.getChats(user.id)
      setChats(storedChats)

      const endpoint = 'http://192.168.1.121:4000'
      socket.current = io(endpoint, {
        query: { token }
      })

      socket.current.on('chat', (msg, fn) => {
        notify('New Message', `You received a new message from ${msg.sender.name}`)

        addIncomingMessage(msg)
        fn('received')
      })

      socket.current.on('offerReceived', (msg, fn) => {
        notify('Someone offered help', `${msg.helpingUserName} wants to help!`)
      })
    }

    if (token) {
      if (socket.current) socket.current.close()
      reactotron.log('inside load chats')
      load()
    }
  }, [token])

  function convertIncomingMessage (msg) {
    return {
      _id: msg.id,
      text: msg.text,
      createdAt: msg.created_at,
      user: {
        _id: msg.sender_id
      }
    }
  }

  function createEmptyChat (interlocutorUser) {
    setChats(oldChats => {
      const chat = {
        interlocutorUser,
        messages: []
      }

      oldChats.push(chat)
      deviceStorage.updateChats(oldChats, user.id)
      return [...oldChats]
    })
  }

  function addIncomingMessage (msg) {
    setChats(oldChats => {
      reactotron.log({ oldChats })

      const index = oldChats.findIndex((c) => c.interlocutorUser.id === msg.sender_id)
      console.log(msg)
      let chat
      if (index === -1) {
        chat = {
          interlocutorUser: msg.sender,
          messages: []
        }
      } else {
        chat = oldChats[index]
        oldChats.splice(index, 1)
      }

      chat.messages.push(convertIncomingMessage(msg))
      oldChats.push(chat)

      reactotron.log({ oldChats })
      deviceStorage.updateChats(oldChats, user.id)

      return [...oldChats]
    })
  }

  function sendMessage (msg, interlocutorId) {
    // reformat message for DB
    const msgForDb = {
      sender: user.id,
      recipient: interlocutorId,
      text: msg.text
    }

    // send message
    reactotron.log(msgForDb)
    socket.current.emit('chat', msgForDb)
  }

  function addOutgoingMessage (msg, interlocutorId) {
    sendMessage(msg, interlocutorId)

    // add message to chats
    setChats(oldChats => {
      const index = oldChats.findIndex((c) => c.interlocutorUser.id === interlocutorId)

      let chat
      if (index === -1) {
        chat = {
          interlocutorUser: { id: interlocutorId },
          messages: []
        }
      } else {
        chat = oldChats[index]
        oldChats.splice(index, 1)
      }

      chat.messages.push(msg)
      oldChats.push(chat)

      deviceStorage.updateChats(oldChats, user.id)

      return [...oldChats]
    })
  }

  async function logout () {
    try {
      LoginManager.logOut()
      await GoogleSignin.revokeAccess()
      await GoogleSignin.signOut()
      await axios.post('http://192.168.1.121:4000/auth/logout', { token })
      await deviceStorage.deleteToken(setToken)
      await deviceStorage.deleteUser(setUser)
      if (socket.current) socket.current.close()
    } catch (error) {
      reactotron.log(error)
    }
  }

  const appContext = {
    token,
    setToken,
    firstUse,
    setFirstUse,
    isLoading,
    setIsLoading,
    user,
    setUser,
    logout,
    location,
    setLocation,
    chats,
    addOutgoingMessage,
    createEmptyChat
  }

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false)
    }, 1000)
  }, [])

  if (isLoading) {
    return <Splash />
  }

  return (
    <AppContext.Provider value={appContext}>
      <NavigationContainer>
        <RootStackScreen token={token} firstUse={firstUse} />
      </NavigationContainer>
    </AppContext.Provider>

  )
}

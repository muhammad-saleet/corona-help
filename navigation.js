import React from 'react'
import { AppContext } from './shared/context'

import { styles } from './shared/styles'
import Icon from 'react-native-vector-icons/MaterialIcons'

import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator, DrawerContentScrollView, DrawerItem, DrawerItemList } from '@react-navigation/drawer'

import { Login } from './screens/Login'
import { CreateAccount } from './screens/CreateAccount'
import { ResetPass } from './screens/ResetPass'
import { RequestOrOffer } from './screens/RequestOrOffer'
import { GetLocation } from './screens/GetLocation'
import { CreateRequest } from './screens/CreateRequest'
import { EditHelpSettings } from './screens/EditHelpSettings'
import { Requests } from './screens/Requests'
import { ViewRequest } from './screens/ViewRequest'
import { EditRequest } from './screens/EditRequest'
import { ViewUserProfile } from './screens/ViewUserProfile'
import { ViewUserPosts } from './screens/ViewUserPosts'
import { EditUserProfile } from './screens/EditUserProfile'
import { Helping } from './screens/Helping'
import { ChatList } from './screens/ChatList'
import { ViewChat } from './screens/ViewChat'

const AuthStack = createStackNavigator()
const AuthStackScreen = () => (
  <AuthStack.Navigator
    initialRouteName='Login'
    screenOptions={{
      headerStyle: styles.header,
      headerTintColor: '#2f2f2f',
      headerTitleStyle: styles.headerTitle
    }}
  >
    <AuthStack.Screen
      name='Login'
      component={Login}
      options={{ title: 'Login' }}
    />
    <AuthStack.Screen
      name='CreateAccount'
      component={CreateAccount}
      options={{ title: 'Create Account' }}
    />
    <AuthStack.Screen
      name='ResetPass'
      component={ResetPass}
      options={{ title: 'Reset Password' }}
    />
  </AuthStack.Navigator>
)

const FirstUseStack = createStackNavigator()
const FirstUseStackScreen = () => (
  <FirstUseStack.Navigator
    initialRouteName='RequestOrOffer'
    screenOptions={{
      headerStyle: styles.header,
      headerTintColor: '#2f2f2f',
      headerTitleStyle: styles.headerTitle
    }}
  >
    <FirstUseStack.Screen
      name='RequestOrOffer'
      component={RequestOrOffer}
      options={{ title: 'Looking for help?' }}
    />
    <FirstUseStack.Screen
      name='GetLocation'
      component={GetLocation}
      options={{ title: 'Location Access', headerShown: false }}
    />
    <FirstUseStack.Screen
      name='CreateRequest'
      component={CreateRequest}
      options={{ title: 'Post a Request' }}
    />
    <FirstUseStack.Screen
      name='EditHelpSettings'
      component={EditHelpSettings}
      options={{ title: 'Edit Help Settings' }}
    />
  </FirstUseStack.Navigator>
)

const AppStack = createStackNavigator()
const AppStackScreen = ({ navigation }) => (
  <AppStack.Navigator
    initialRouteName='Requests'
    screenOptions={{
      headerStyle: styles.header,
      headerTintColor: '#2f2f2f',
      headerTitleStyle: styles.headerTitle
    }}
  >
    <AppStack.Screen
      name='Requests'
      component={Requests}
      options={{
        title: 'Requests',
        headerLeft: () =>
          <Icon
            name='menu'
            size={28}
            onPress={() => navigation.toggleDrawer()}
            style={styles.icon}
          />
      }}
    />
    <AppStack.Screen name='ViewRequest' component={ViewRequest} options={{ title: 'View Request' }} />
    <AppStack.Screen name='EditRequest' component={EditRequest} options={{ title: 'Edit Request' }} />
    <AppStack.Screen name='ViewUserProfile' component={ViewUserProfile} options={{ title: 'Profile' }} />
    <AppStack.Screen name='CreateRequest' component={CreateRequest} options={{ title: 'Create Request' }} />
    <AppStack.Screen name='ViewUserPosts' component={ViewUserPosts} options={{ title: 'My Posts' }} />
    <AppStack.Screen name='EditUserProfile' component={EditUserProfile} options={{ title: 'Edit Profile' }} />
    <AppStack.Screen name='EditHelpSettings' component={EditHelpSettings} options={{ title: 'Help Settings' }} />
    <AppStack.Screen name='Helping' component={Helping} options={{ title: 'Helping' }} />
    <AppStack.Screen name='ChatList' component={ChatList} options={{ title: 'Chat' }} />
    <AppStack.Screen name='ViewChat' component={ViewChat} options={({ route }) => ({ title: route.params.interlocutorUser.name })} />
  </AppStack.Navigator>
)

function CustomDrawerContent (props) {
  const { logout } = React.useContext(AppContext)

  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem
        label='Post a Request'
        onPress={() => props.navigation.navigate('CreateRequest')}
      />
      <DrawerItem
        label='Helping'
        onPress={() => props.navigation.navigate('Helping')}
      />
      <DrawerItem
        label='My Posts'
        onPress={() => props.navigation.navigate('ViewUserPosts')}
      />
      <DrawerItem
        label='Profile'
        onPress={() => props.navigation.navigate('ViewUserProfile')}
      />
      <DrawerItem
        label='Chat'
        onPress={() => props.navigation.navigate('ChatList')}
      />
      <DrawerItem
        label='Help Settings'
        onPress={() => props.navigation.navigate('EditHelpSettings')}
      />

      <DrawerItem
        label='Logout'
        onPress={() => logout()}
      />
      {/*                  <DrawerItem
        label='First Use'
        onPress={() => {
          deviceStorage.changeFirstUse('true', setFirstUse)
          logout()
        }}
      />
      <DrawerItem
        label='delete chats'
        onPress={() => {
          deviceStorage.deleteChats(user.id.toString())
        }}
      /> */}
    </DrawerContentScrollView>
  )
}

const Drawer = createDrawerNavigator()
const DrawerScreen = () => {
  return (
    <Drawer.Navigator
      drawerContent={props => <CustomDrawerContent {...props} />}
    >
      <Drawer.Screen
        name='AppStackScreen'
        component={AppStackScreen}
        options={{ title: 'Browse Requests' }}
      />
    </Drawer.Navigator>
  )
}

const RootStack = createStackNavigator()
export const RootStackScreen = ({ token, firstUse }) => {
  if (!token) {
    return (
      <RootStack.Navigator headerMode='none'>
        <RootStack.Screen
          name='Auth'
          component={AuthStackScreen}
          options={{
            animationEnabled: false
          }}
        />
      </RootStack.Navigator>
    )
  } else if (firstUse === 'true') {
    return (
      <RootStack.Navigator headerMode='none'>
        <RootStack.Screen
          name='App'
          component={FirstUseStackScreen}
          options={{
            animationEnabled: false
          }}
        />
      </RootStack.Navigator>
    )
  } else {
    return (
      <RootStack.Navigator headerMode='none'>
        <RootStack.Screen
          name='App'
          component={DrawerScreen}
          options={{
            animationEnabled: false
          }}
        />
      </RootStack.Navigator>
    )
  }
}

import { Dimensions, StyleSheet } from 'react-native'

export const colors = {
  primary: '#3e40e2',
  primaryDark: '#191ba9',
  secondary: '#77b8eb',
  white: '#f0f0f0',
  black: '#2f2f2f',
  red: '#ff5959',
  grey: '#696969',
  lightgrey: '#D3D3D3'
}

const screenHeight = Math.round(Dimensions.get('window').height)
const screenWidth = Math.round(Dimensions.get('window').width)

export const sizes = {
  screenHeight,
  screenWidth,
  mapHeight: Math.round(screenHeight / 2),
  mapWidth: screenWidth - 50
}

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  button: {
    marginVertical: 80,
    marginHorizontal: 10,
    backgroundColor: colors.primary
  },
  buttonText: {
    color: colors.black
  },
  header: {
    backgroundColor: colors.secondary
  },
  headerTitle: {
    fontFamily: 'Nunito-Bold'
  },
  icon: {
    position: 'absolute',
    left: 16
  },
  logo: {
    fontWeight: 'bold',
    fontSize: 50,
    color: '#4a7eb8',
    marginBottom: 40,
    textAlign: 'center'
  },
  errorText: {
    color: 'crimson',
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 6,
    marginLeft: 10,
    textAlign: 'left'
  }
})

import AsyncStorage from '@react-native-community/async-storage'
import reactotron from 'reactotron-react-native'

const deviceStorage = {
  async addToken (token, setToken) {
    try {
      await AsyncStorage.setItem('token', token)
      setToken(token)
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  },

  async loadToken (setToken) {
    try {
      const value = await AsyncStorage.getItem('token')
      if (value !== null) setToken(value)
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  },

  async deleteToken (setToken) {
    try {
      await AsyncStorage.removeItem('token')
      setToken(null)
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  },

  async addUser (user, setUser) {
    try {
      await AsyncStorage.setItem('user', JSON.stringify(user))

      reactotron.log('inside add user')
      reactotron.log(JSON.stringify(user))

      setUser(user)
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  },

  async loadUser (setUser) {
    try {
      const value = await AsyncStorage.getItem('user')

      reactotron.log('inside load user')
      reactotron.log(JSON.parse((value)))

      if (value !== null) setUser(JSON.parse(value))
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  },

  async deleteUser (setUser) {
    try {
      await AsyncStorage.removeItem('user')

      reactotron.log('inside delete user')

      setUser({})
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  },

  async changeFirstUse (firstUse, setFirstUse) {
    try {
      await AsyncStorage.setItem('firstUse', firstUse)
      setFirstUse(firstUse)
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  },

  async loadFirstUse (setFirstUse) {
    try {
      const value = await AsyncStorage.getItem('firstUse')
      if (value !== null) setFirstUse(value)
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  },

  async updateChats (newChats, userId) {
    try {
      reactotron.log('inside update chats')
      reactotron.log(newChats)

      await AsyncStorage.setItem('chats' + userId, JSON.stringify(newChats))
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  },

  async getChats (userId) {
    try {
      const chats = await AsyncStorage.getItem('chats' + userId)
      reactotron.log('inside get chats')
      reactotron.log(chats)

      if (chats !== null) return JSON.parse(chats)
      else return []
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  },

  async deleteChats (userId) {
    try {
      reactotron.log('inside delete chats')
      await AsyncStorage.removeItem('chats' + userId)
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message)
    }
  }
}

export default deviceStorage

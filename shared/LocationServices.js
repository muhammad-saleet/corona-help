import { PermissionsAndroid, Platform, ToastAndroid } from 'react-native'
import reactotron from 'reactotron-react-native'
import Geolocation from 'react-native-geolocation-service'

async function hasLocationPermission () {
  if (Platform.OS === 'ios' ||
    (Platform.OS === 'android' && Platform.Version < 23)) {
    return true
  }

  return PermissionsAndroid.check(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
  )
}

async function requestPermission () {
  const status = await PermissionsAndroid.request(
    'android.permission.ACCESS_FINE_LOCATION',
    {
      title: 'Allow Location Access',
      message:
        'Corona help needs access to your location' +
        'to connect you to people in your area',
      buttonNegative: 'Cancel',
      buttonPositive: 'OK'
    }).then(response => response === 'granted')

  reactotron.log({ status: status })

  if (status === PermissionsAndroid.RESULTS.GRANTED) return true

  if (status === PermissionsAndroid.RESULTS.DENIED) {
    ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG)
  } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
    ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG)
  }
  return false
}

async function getLocation (setLocation) {
  if (!await hasLocationPermission() && !await requestPermission()) return

  await Geolocation.getCurrentPosition(
    (position) => {
      setLocation({
        longitude: position.coords.longitude,
        latitude: position.coords.latitude
      })
    },
    (error) => {
      reactotron.log(error)
    },
    {
      enableHighAccuracy: true,
      timeout: 15000,
      maximumAge: 10000,
      distanceFilter: 50,
      forceRequestLocation: true
    }
  )
}

export const LocationServices = {
  hasLocationPermission,
  requestPermission,
  getLocation
}

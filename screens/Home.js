import {
  PermissionsAndroid,
  Platform,
  ToastAndroid,
  View,
  FlatList,
  StyleSheet,
  ActivityIndicator, RefreshControl
} from 'react-native'

import React, { useEffect, useState } from 'react'
import Geolocation from 'react-native-geolocation-service'
import reactotron from 'reactotron-react-native'
import { AppContext } from '../shared/context'
import { RequestCard } from '../components/RequestCard'
import axios from 'axios'

export function Home ({ navigation }) {
  const { locationHandle } = React.useContext(AppContext)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const callGetLocation = async () => {
      await getLocation()
    }
    callGetLocation()
  }, [])

  async function hasLocationPermission () {
    if (Platform.OS === 'ios' ||
      (Platform.OS === 'android' && Platform.Version < 23)) {
      return true
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    )

    if (hasPermission) return true

    const status = await PermissionsAndroid.request(
      'android.permission.ACCESS_FINE_LOCATION',
      {
        title: 'Allow Location Access',
        message:
          'Corona help needs access to your location' +
          'to connect you to people in your area',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK'
      }).then(response => response === 'granted')

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG)
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG)
    }
    return false
  }

  async function getLocation () {
    const hasPermission = await hasLocationPermission()
    if (!hasPermission) return

    setLoading(true)
    await Geolocation.getCurrentPosition(
      (position) => {
        locationHandle.setLocation({
          longitude: position.coords.longitude,
          latitude: position.coords.latitude
        })
        setLoading(false)
      },
      (error) => {
        locationHandle.setLocation(error)
        setLoading(false)
        reactotron.log({ getLocation: error })
      },
      {
        enableHighAccuracy: true,
        timeout: 15000,
        maximumAge: 10000,
        distanceFilter: 50,
        forceRequestLocation: true
      }
    )
  }

  const [data, setData] = useState([])
  const [page, setPage] = useState(1)
  // todo: fix bug: on first refresh no results are shown
  const [refreshing, setRefreshing] = useState(false)

  useEffect(() => {
    let didCancel = false
    const fetchData = async () => {
      const response = await axios.get(
        'https://5e9e4f8afb467500166c3d7e.mockapi.io/corona/help/posts?page=' + page + '&limit=10'
      )
      if (!didCancel) {
        setData(data.concat(response.data))
      }
    }
    fetchData()
    return () => {
      didCancel = true
    }
  }, [page])

  function handleLoadMore () {
    setPage(page + 1)
  }

  function handleRefresh () {
    setRefreshing(true)
    setData([])
    setPage(1)
    setRefreshing(false)
  }

  function navigateToViewPost (item) {
    navigation.navigate('ViewPost', item)
  }

  function renderCard ({ item }) {
    return (
      <View style={{ flex: 1 }}>
        <RequestCard item={item} navigateToViewPost={navigateToViewPost} />
      </View>
    )
  }

  function renderFooter () {
    return (
      <View style={styles.loader}>
        <ActivityIndicator size='large' />
      </View>
    )
  }

  return (
    <View>
      <FlatList
        data={data}
        renderItem={renderCard}
        keyExtractor={(item, index) => index.toString()}
        onEndReached={handleLoadMore}
        onEndReachedThreshold={0.5}
        ListFooterComponent={renderFooter}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={handleRefresh}
          />
        }
      />
    </View>
  )
}

const styles = StyleSheet.create({
  image: {
    height: 150,
    width: 150
  },
  loader: {
    alignItems: 'center',
    marginBottom: 30
  }
})

import { ScrollView, Text, View, StyleSheet } from 'react-native'
import React from 'react'
import reactotron from 'reactotron-react-native'
import { LocationView } from '../components/LocationView'
import { colors } from '../shared/styles'
import { TagList } from '../components/TagList'
import FormButton from '../components/FormButton'

export const ViewHelpingRequest = ({ navigation, route }) => {
  const data = route.params

  function handleFinish () {
    reactotron.log('handleFinish')
  }

  function handleExit () {
    reactotron.log('handleExit')
  }

  function handleChat () {
    reactotron.log('handleChat')
  }

  return (
    <ScrollView>
      <LocationView
        isPinDraggable={false}
        location={{
          longitude: -0.1337,
          latitude: 51.50998
        }}
        title={'' + data.name + "'s" + ' location'}
        description="location provided by the requests's owner"
      />

      <View style={styles.outerContainer}>

        <View>
          <Text style={styles.title}>
            {data.title}
          </Text>
        </View>

        <Text style={styles.name}>
          {data.name}
        </Text>

        <View style={styles.data}>
          <Text style={styles.date}>
            Posted on {data.date.slice(0, 10)}
          </Text>
        </View>

        <View style={styles.description}>
          <Text style={{ fontSize: 16 }}>
            {data.description}
          </Text>
        </View>

        <View style={{ marginHorizontal: -25, marginVertical: 10 }}>
          <TagList
            tags={[
              { text: 'help', key: 1 },
              { text: 'mental', key: 2 },
              { text: 'urgent', key: 3 }
            ]}
            editMode={false}
          />
        </View>

        <View style={{ marginTop: 30, marginBottom: 10 }}>
          <FormButton
            title='Chat1'
            onPress={handleChat}
            buttonColor={colors.primary}
            titleColor={colors.white}
          />
        </View>

        <View style={{ marginTop: 10, marginBottom: 10 }}>
          <FormButton
            title='Finish Helping'
            onPress={handleFinish}
            buttonColor={colors.primary}
            titleColor={colors.white}
          />
        </View>

        <View style={{ marginTop: 10, marginBottom: 25 }}>
          <FormButton
            title='Exit Helping'
            onPress={handleExit}
            buttonColor={colors.red}
            titleColor={colors.black}
          />
        </View>

      </View>

    </ScrollView>
  )
}

export const styles = StyleSheet.create({
  outerContainer: {
    marginHorizontal: 25,
    paddingHorizontal: 5
  },
  name: {
    color: colors.primary,
    fontSize: 14
  },
  data: {
    borderBottomWidth: 1,
    borderBottomColor: '#D3D3D3',
    paddingBottom: 5
  },
  title: {
    fontWeight: 'bold',
    fontSize: 22
  },
  date: {
    fontSize: 12,
    color: colors.black,
    fontStyle: 'italic'
  },
  description: {
    borderBottomWidth: 1,
    borderBottomColor: '#D3D3D3',
    paddingBottom: 8,
    paddingTop: 12
  }

})

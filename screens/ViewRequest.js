import { ScrollView, Text, View, StyleSheet } from 'react-native'
import React, { useContext } from 'react'
import reactotron from 'reactotron-react-native'
import { LocationView } from '../components/LocationView'
import { colors } from '../shared/styles'
import { TagList } from '../components/TagList'
import FormButton from '../components/FormButton'
import { AppContext } from '../shared/context'
import axios from 'axios'

export const ViewRequest = ({ navigation, route }) => {
  const { user } = useContext(AppContext)
  const { item, UserIsHelping } = route.params

  let postState
  if (user.id === item.user.id) {
    postState = 'usersPost'
  } else if (UserIsHelping) {
    postState = 'userIsHelping'
  } else {
    postState = 'post'
  }

  // todo: handle actions inside view request
  function handleHelp () {
    reactotron.log('handleHelp')
  }

  function handleIgnore () {
    reactotron.log('handleIgnore')
  }

  function handleEdit () {
    reactotron.log('handleEdit')
    navigation.navigate('EditRequest', { item })
  }

  function handleExit () {
    reactotron.log('handleExit')
  }

  function handleChat () {
    reactotron.log('handleChat')
  }

  function renderButtons () {
    if (postState === 'post') {
      return (
        <>
          <View style={{ marginTop: 30, marginBottom: 10 }}>
            <FormButton
              title='Help'
              onPress={handleHelp}
              buttonColor={colors.primary}
              titleColor={colors.white}
            />
          </View>

          <View style={{ marginTop: 10, marginBottom: 40 }}>

            <FormButton
              title='Ignore'
              onPress={handleIgnore}
              buttonColor={colors.lightgrey}
              titleColor={colors.black}
            />
          </View>
        </>
      )
    } else if (postState === 'usersPost') {
      return (
        <View style={{ marginTop: 10, marginBottom: 40 }}>
          <FormButton
            title='Edit request'
            onPress={handleEdit}
            buttonColor={colors.primary}
            titleColor={colors.white}
          />
        </View>
      )
    } else {
      return (
        <>
          <View style={{ marginTop: 30, marginBottom: 10 }}>
            <FormButton
              title='Exit'
              onPress={handleExit}
              buttonColor={colors.primary}
              titleColor={colors.white}
            />
          </View>

          <View style={{ marginTop: 10, marginBottom: 40 }}>
            <FormButton
              title='Chat'
              onPress={handleChat}
              buttonColor={colors.green}
              titleColor={colors.white}
            />
          </View>
        </>
      )
    }
  }

  return (
    <ScrollView>
      <LocationView
        isPinDraggable={false}
        location={{
          longitude: item.location.longitude,
          latitude: item.location.latitude
        }}
        title={'' + item.user.name + "'s" + ' location'}
        description="location provided by the requests's owner"
      />

      <View style={styles.outerContainer}>

        <View>
          <Text style={styles.title}>
            {item.title}
          </Text>
        </View>

        <Text style={styles.name}>
          {item.user.name}
        </Text>

        <View style={styles.data}>
          <Text style={styles.date}>
          Posted on {item.created_at.slice(0, 10)}
          </Text>
        </View>

        <View style={styles.description}>
          <Text style={{ fontSize: 16 }}>
            {item.description}
          </Text>
        </View>

        <View style={{ marginHorizontal: -25, marginVertical: 10 }}>
          <TagList
            tags={item.tags.map((item) => {
              return { text: item, key: Math.floor(Math.random() * (2 ** 32)).toString() }
            })}
            editMode={false}
          />
        </View>

        {renderButtons()}

      </View>

    </ScrollView>
  )
}

export const styles = StyleSheet.create({
  outerContainer: {
    marginHorizontal: 25,
    paddingHorizontal: 5
  },
  name: {
    color: colors.primary,
    fontSize: 14
  },
  data: {
    borderBottomWidth: 1,
    borderBottomColor: '#D3D3D3',
    paddingBottom: 5
  },
  title: {
    fontWeight: 'bold',
    fontSize: 22
  },
  date: {
    fontSize: 12,
    color: colors.black,
    fontStyle: 'italic'
  },
  description: {
    borderBottomWidth: 1,
    borderBottomColor: '#D3D3D3',
    paddingBottom: 8,
    paddingTop: 12
  }

})

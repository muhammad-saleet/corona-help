import { ScrollView, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import reactotron from 'reactotron-react-native'
import * as Yup from 'yup'
import { Formik } from 'formik'
import FormButton from '../components/FormButton'
import { colors } from '../shared/styles'
import { TagList } from '../components/TagList'
import { LocationView } from '../components/LocationView'
import { AppContext } from '../shared/context'
import { TextInput } from 'react-native-paper'
import { PopUp } from '../components/Popup'
import axios from 'axios'

export const EditRequest = ({ navigation, route }) => {
  const { item } = route.params
  const { location, user } = React.useContext(AppContext)
  const [tags, setTags] = useState(dbTagsToReactTags(item.tags))
  const [postLocation, setPostLocation] = useState(item.location)
  const [showConfirmDelete, setShowConfirmDelete] = useState(false)
  const [updatedLocation, setUpdatedLocation] = useState(false)

  reactotron.log(item)

  function deletePost () {
    reactotron.log('post deleted')
    setShowConfirmDelete(false)
    navigation.popToTop()
  }
  function dbTagsToReactTags (tags) {
    return tags.map((tag) => ({ text: tag, key: Math.floor(Math.random() * (2 ** 32)).toString() }))
  }

  async function handleSubmit (values) {
    if (values.title.length > 0 && values.description.length > 0) {
      const tagTextList = tags.map((tag) => tag.text)
      values = { ...values, userId: user.id, state: 1, location: postLocation, tags: tagTextList }

      await axios.put('http://192.168.1.121:4000/posts/' + item.id, values)

      navigation.navigate('ViewUserPosts')
    }
  }

  const validationSchema = Yup.object().shape({
    title: Yup.string()
      .label('Title')
      .required('You need to give your post a title!')
      .min(5, 'Your title must be 5-25 characters')
      .max(50, 'Your title must be 5-50 characters'),
    description: Yup.string()
      .label('Description')
      .required()
      .min(4, 'Description must be 5-200 characters')
      .max(200, 'Description must be 5-200 characters')
  })

  return (
    <ScrollView>

      <LocationView
        isPinDraggable
        location={postLocation}
        setLocationOnPinDrag={setPostLocation}
        title='Your Location'
        description='Tap and hold to drag pin'
      />
      {!updatedLocation &&
        <TouchableOpacity onPress={() => {
          setUpdatedLocation(true)
          setPostLocation(location)
        }}
        >
          <Text style={{ marginHorizontal: 30, marginTop: -10, marginBottom: 10, color: colors.primary }}>
          Update location
          </Text>
        </TouchableOpacity>}

      {updatedLocation &&
        <TouchableOpacity onPress={() => {
          setUpdatedLocation(false)
          setPostLocation(item.location)
        }}
        >
          <Text style={{ marginHorizontal: 30, marginTop: -10, marginBottom: 10, color: colors.primary }}>
          Revert
          </Text>
        </TouchableOpacity>}

      <Formik
        initialValues={{ title: item.title, description: item.description }}
        onSubmit={values => {
          handleSubmit(values)
        }}
        validationSchema={validationSchema}
      >
        {({
          handleChange,
          values,
          handleSubmit,
          errors,
          isValid,
          touched,
          handleBlur,
          isSubmitting
        }) => (
          <View style={{ flex: 1 }}>
            <TextInput
              style={{ marginHorizontal: 25, marginVertical: 15 }}
              mode='outlined'
              name='title'
              label='Title'
              placeholder="Your post's title"
              value={values.title}
              onChangeText={handleChange('title')}
              onBlur={handleBlur('title')}
              error={touched.title && errors.title}
            />

            <TextInput
              style={{ marginHorizontal: 25, marginVertical: 15 }}
              mode='outlined'
              multiline
              numberOfLines={5}
              name='description'
              label='Description'
              placeholder='Give a brief description to your post'
              value={values.description}
              onChangeText={handleChange('description')}
              onBlur={handleBlur('description')}
              error={touched.description && errors.description}
            />

            <TagList
              tags={tags}
              setTags={setTags}
              editMode
            />

            <View style={{ marginHorizontal: 25, marginBottom: 10, marginTop: 40 }}>
              <FormButton
                title='Save Changes'
                onPress={handleSubmit}
                buttonColor={colors.primary}
                titleColor={colors.white}
                disabled={!isValid || isSubmitting}
                loading={isSubmitting}
              />
            </View>

            <View style={{ marginHorizontal: 25, marginTop: 10, marginBottom: 25 }}>
              <FormButton
                title='Delete Post'
                onPress={() => setShowConfirmDelete(true)}
                buttonColor={colors.red}
                titleColor={colors.black}
                disabled={!isValid || isSubmitting}
              />
            </View>

            <PopUp
              text='Are you sure you want to delete this post?'
              leftBtnText='OK'
              rightBtnText='Cancel'
              leftBtnTextColor={colors.black}
              rightBtnTextColor={colors.black}
              leftBtnColor={colors.red}
              rightBtnColor={colors.secondary}
              leftBtnOnPress={deletePost}
              rightBtnOnPress={() => setShowConfirmDelete(false)}
              visibility={showConfirmDelete}
              changeVisibility={setShowConfirmDelete}
            />

          </View>
        )}
      </Formik>
    </ScrollView>
  )
}

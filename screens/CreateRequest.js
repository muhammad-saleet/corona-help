import { ScrollView, View } from 'react-native'
import React, { useState } from 'react'
import * as Yup from 'yup'
import { Formik } from 'formik'
import FormButton from '../components/FormButton'
import { colors } from '../shared/styles'
import { TagList } from '../components/TagList'
import { LocationView } from '../components/LocationView'
import { AppContext } from '../shared/context'
import { TextInput } from 'react-native-paper'
import deviceStorage from '../shared/deviceStorage'
import axios from 'axios'

export const CreateRequest = ({ navigation }) => {
  const { location, setFirstUse, user } = React.useContext(AppContext)
  const [tags, setTags] = useState([])
  const [postLocation, setPostLocation] = useState(location)

  async function handleSubmit (values) {
    if (values.title.length > 0 && values.description.length > 0) {
      const tagTextList = tags.map((tag) => tag.text)
      values = { ...values, userId: user.id, state: 1, location: postLocation, tags: tagTextList }

      await axios.post('http://192.168.1.121:4000/posts/', values)
      await deviceStorage.changeFirstUse('false', setFirstUse)

      navigation.navigate('ViewUserPosts')
    }
  }

  const validationSchema = Yup.object().shape({
    title: Yup.string()
      .label('Title')
      .required('You need to give your post a title!')
      .min(5, 'Your title must be 5-50 characters')
      .max(50, 'Your title must be 5-50 characters'),
    description: Yup.string()
      .label('Description')
      .required()
      .min(5, 'Description must be 5-200 characters')
      .max(200, 'Description must be 5-200 characters')
  })

  return (
    <ScrollView>

      <LocationView
        isPinDraggable
        location={postLocation}
        setLocationOnPinDrag={setPostLocation}
        title='Your location'
        description='Tap and hold to drag pin'
      />

      <Formik
        initialValues={{ title: '', description: '' }}
        onSubmit={values => {
          handleSubmit(values)
        }}
        validationSchema={validationSchema}
      >
        {({
          handleChange,
          values,
          handleSubmit,
          errors,
          isValid,
          touched,
          handleBlur,
          isSubmitting
        }) => (
          <View style={{ flex: 1 }}>
            <TextInput
              style={{ marginHorizontal: 25, marginVertical: 15 }}
              mode='outlined'
              name='title'
              label='Title'
              placeholder="Your post's title"
              value={values.title}
              onChangeText={handleChange('title')}
              onBlur={handleBlur('title')}
              error={touched.title && errors.title}
            />

            <TextInput
              style={{ marginHorizontal: 25, marginVertical: 15 }}
              mode='outlined'
              multiline
              numberOfLines={5}
              name='description'
              label='Description'
              placeholder='Give a brief description to your post'
              value={values.description}
              onChangeText={handleChange('description')}
              onBlur={handleBlur('description')}
              error={touched.description && errors.description}
            />

            <TagList
              tags={tags}
              setTags={setTags}
              editMode
            />

            <View style={{ margin: 25, marginBottom: 50 }}>
              <FormButton
                title='Post'
                onPress={handleSubmit}
                buttonColor={colors.primary}
                titleColor={colors.white}
                disabled={!isValid || isSubmitting}
                loading={isSubmitting}
              />
            </View>
          </View>
        )}
      </Formik>
    </ScrollView>
  )
}

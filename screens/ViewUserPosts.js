import { FlatList, RefreshControl, View } from 'react-native'
import React, { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import { AppContext } from '../shared/context'
import { UserRequestCard } from '../components/UserRequestCard'

export const ViewUserPosts = ({ navigation }) => {
  const { user } = useContext(AppContext)
  const [posts, setPosts] = useState([])
  const [refreshing, setRefreshing] = useState(false)

  async function fetchData () {
    const response = await axios.get(
      'http://192.168.1.121:4000/posts/user/' + user.id
    )

    setPosts(response.data)
  }

  // todo: refreshing not working
  useEffect(() => {
    fetchData()
  }, [])

  // accept offer of help
  async function accept (postId, helpingUserId) {
    const request = { postId, helpingUserId }
    await axios.post('http://192.168.1.121:4000/posts/accept/', request)
    await fetchData()
  }

  // reject offer of help
  async function reject (postId, helpingUserId) {
    const request = { postId, helpingUserId }
    await axios.post('http://192.168.1.121:4000/posts/reject/', request)
    await fetchData()
  }

  // finish request
  async function finish (postId, helpingUserId) {
    const request = { postId, helpingUserId }
    await axios.post('http://192.168.1.121:4000/posts/finish/', request)

    setPosts((oldPosts) => {
      return oldPosts.filter((p) => p.id !== postId)
    })
  }

  // exit request
  async function exit (postId, helpingUserId) {
    const request = { postId, helpingUserId }
    await axios.post('http://192.168.1.121:4000/posts/exit/', request)

    setPosts((oldPosts) => {
      return oldPosts.filter((p) => p.id !== postId)
    })
  }

  function handleRefresh () {
    setRefreshing(true)
    setPosts([])
    setRefreshing(false)
  }

  function renderCard ({ item }) {
    return (
      <View style={{ flex: 1 }}>
        <UserRequestCard
          item={item}
          navigateToViewRequest={() => { navigation.navigate('ViewRequest', { item, UserIsHelping: true }) }}
          navigateToUserProfile={() => navigation.navigate('ViewUserProfile', item.user)}
          accept={accept}
          reject={reject}
          finish={finish}
          exit={exit}
        />
      </View>
    )
  }

  return (
    <View>
      <FlatList
        data={posts}
        renderItem={renderCard}
        keyExtractor={(item, index) => index.toString()}
        onEndReachedThreshold={0.5}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={handleRefresh}
          />
        }
      />
    </View>
  )
}

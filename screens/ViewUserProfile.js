import { ScrollView, Text, View, StyleSheet } from 'react-native'
import React, { useContext, useState } from 'react'
import { Avatar } from 'react-native-elements'
import { colors, sizes } from '../shared/styles'
import reactotron from 'reactotron-react-native'
import FormButton from '../components/FormButton'
import ImagePicker from 'react-native-image-picker'
import { AppContext } from '../shared/context'

export const ViewUserProfile = ({ navigation, route }) => {
  const { user, setUser } = useContext(AppContext)
  const [profileUser, setProfileUser] = useState(route.params || user)
  const isUserProfile = !route.params || route.params.id === user.id

  const options = {
    title: 'Select Avatar',
    storageOptions: {
      skipBackup: true,
      path: 'images'
    }
  }

  function getNewAvatar () {
    ImagePicker.showImagePicker(options, (response) => {
      reactotron.log('Response = ', response)

      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        const source = response.uri
        setUser({ ...user, avatar: source })
        setProfileUser({ ...profileUser, avatar: source })
      }
    })
  }

  function renderAvatar () {
    if (isUserProfile) {
      return (
        <Avatar
          avatarStyle={styles.avatar}
          rounded
          showEditButton
          editButton={{ size: 35 }}
          size='xlarge'
          icon={{
            name: 'person',
            size: 35,
            color: colors.grey
          }}
          source={{
            uri: profileUser.avatar
          }}
          onPress={() => reactotron.log('avatar pressed')}
          onEditPress={getNewAvatar}
        />
      )
    } else {
      return (
        <Avatar
          avatarStyle={styles.avatar}
          rounded
          size='xlarge'
          icon={{
            name: 'person',
            size: 35,
            color: colors.grey
          }}
          source={{
            uri: profileUser.avatar
          }}
        />
      )
    }
  }

  return (
    <ScrollView>
      <View style={styles.avatarContainer}>
        {renderAvatar()}
      </View>

      <View style={styles.propertyRow}>
        <Text style={styles.property}>
          Name
        </Text>
        <Text style={styles.propertyValue}>
          {profileUser.name || '-'}
        </Text>
      </View>

      <View style={styles.propertyRow}>
        <Text style={styles.property}>
          Email
        </Text>
        <Text style={styles.propertyValue}>
          {profileUser.email || '-'}
        </Text>
      </View>

      <View style={styles.propertyRow}>
        <Text style={styles.property}>
          Birthday
        </Text>
        <Text style={styles.propertyValue}>
          {profileUser.birthday || '-'}
        </Text>
      </View>

      <View style={styles.propertyRow}>
        <Text style={styles.property}>
          Gender
        </Text>
        <Text style={styles.propertyValue}>
          {profileUser.gender || '-'}
        </Text>
      </View>

      <View style={styles.propertyRow}>
        <Text style={styles.property}>
          Profession
        </Text>
        <Text style={styles.propertyValue}>
          {profileUser.profession || '-'}
        </Text>
      </View>
      {isUserProfile &&
        <View style={{ margin: 25, marginBottom: 50 }}>
          <FormButton
            title='Edit'
            buttonColor={colors.primary}
            titleColor={colors.white}
            onPress={() => navigation.navigate('EditUserProfile')}
          />
        </View>}

    </ScrollView>
  )
}

const styles = StyleSheet.create({
  avatarContainer: {
    alignSelf: 'center',
    margin: 25
  },

  avatar: {
    borderWidth: 2,
    borderRadius: 400,
    borderColor: colors.primary
  },

  propertyRow: {
    flex: 1,
    flexDirection: 'row',
    // borderBottomWidth: 1,
    borderBottomColor: colors.lightgrey,
    marginVertical: 20,
    marginLeft: 35
  },

  property: {
    fontSize: 16,
    color: colors.grey
  },

  propertyValue: {
    fontSize: 16,
    color: colors.black,
    position: 'absolute',
    left: 100
  },

  divider: {
    width: sizes.screenWidth - 50,
    alignSelf: 'center'
  }
})

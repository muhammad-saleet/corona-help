import { ActivityIndicator, View } from 'react-native'
import React from 'react'
import { ScreenContainer } from '../components/ScreenContainer'
import { sizes } from '../shared/styles'

export const Splash = () => (
  <ScreenContainer>
    <View style={{ flex: 1, alignItems: 'center', marginTop: Math.round(sizes.screenHeight / 2) - 25 }}>
      <ActivityIndicator size='large' />
    </View>
  </ScreenContainer>
)

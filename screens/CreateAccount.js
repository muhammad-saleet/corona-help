import React, { useState } from 'react'
import { StyleSheet, View, TouchableOpacity } from 'react-native'
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Formik } from 'formik'
import * as Yup from 'yup'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import ErrorMessage from '../components/ErrorMessage'
import { ScreenContainer } from '../components/ScreenContainer'
import { AppContext } from '../shared/context'
import { colors } from '../shared/styles'
import axios from 'axios'
import deviceStorage from '../shared/deviceStorage'
import reactotron from 'reactotron-react-native'

export function CreateAccount ({ navigation }) {
  const { setToken } = React.useContext(AppContext)

  const [passwordVisibility, setPasswordVisibility] = useState(true)
  const [confirmPasswordVisibility, setConfirmPasswordVisibility] = useState(true)
  const [passwordIcon, setPasswordIcon] = useState('visibility')
  const [confirmPasswordIcon, setConfirmPasswordIcon] = useState('visibility')
  const [submitError, setSubmitError] = useState(null)

  async function signUp (user) {
    try {
      const response = await axios.post('http://192.168.1.121:4000/users/', user)
      await deviceStorage.addToken(response.data.token, setToken)
    } catch (error) {
      reactotron.log(error)
      if (error.message === 'Request failed with status code 409') {
        setSubmitError('Email already registered')
      } else if (error.message === 'Request failed with status code 400') {
        setSubmitError('Server Error: Bad input')
      } else {
        setSubmitError('Server Error: Could not sign up')
      }
    }
  }

  const handlePasswordVisibility = () => {
    setPasswordIcon(prevState =>
      prevState === 'visibility' ? 'visibility-off' : 'visibility'
    )
    setPasswordVisibility(prevState => !prevState)
  }

  const handleConfirmPasswordVisibility = () => {
    setConfirmPasswordIcon(prevState =>
      prevState === 'visibility' ? 'visibility-off' : 'visibility'
    )
    setConfirmPasswordVisibility(prevState => !prevState)
  }

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .label('Name')
      .required()
      .min(3, 'Must have at least 3 characters'),
    email: Yup.string()
      .label('Email')
      .email('Enter a valid email')
      .required('Please enter an email'),
    password: Yup.string()
      .label('Password')
      .required()
      .min(6, 'Password must have at least 6 characters '),
    confirmPassword: Yup.string()
      .oneOf([Yup.ref('password')], 'Confirm Password must matched Password')
      .required('Confirm Password is required')
  })

  return (
    <ScreenContainer>
      <Formik
        initialValues={{
          name: '',
          email: '',
          password: '',
          confirmPassword: ''
        }}
        onSubmit={user => {
          signUp(user)
        }}
        validationSchema={validationSchema}
      >
        {({
          handleChange,
          values,
          handleSubmit,
          errors,
          isValid,
          touched,
          handleBlur
        }) => (
          <>
            <FormInput
              name='name'
              value={values.name}
              onChangeText={handleChange('name')}
              placeholder='Full name'
              iconName='person'
              iconColor={colors.primary}
              onBlur={handleBlur('name')}
            />
            <ErrorMessage errorValue={touched.name && errors.name} />
            <FormInput
              name='email'
              value={values.email}
              onChangeText={handleChange('email')}
              placeholder='Email'
              autoCapitalize='none'
              iconName='email'
              iconColor={colors.primary}
              onBlur={handleBlur('email')}
            />
            <ErrorMessage errorValue={touched.email && errors.email} />
            <FormInput
              name='password'
              value={values.password}
              onChangeText={handleChange('password')}
              placeholder='Password'
              iconName='lock'
              iconColor={colors.primary}
              onBlur={handleBlur('password')}
              secureTextEntry={passwordVisibility}
              rightIcon={
                <TouchableOpacity onPress={handlePasswordVisibility}>
                  <Icon name={passwordIcon} size={28} color='grey' />
                </TouchableOpacity>
              }
            />
            <ErrorMessage errorValue={touched.password && errors.password} />
            <FormInput
              name='password'
              value={values.confirmPassword}
              onChangeText={handleChange('confirmPassword')}
              placeholder='Confirm password'
              iconName='lock'
              iconColor={colors.primary}
              onBlur={handleBlur('confirmPassword')}
              secureTextEntry={confirmPasswordVisibility}
              rightIcon={
                <TouchableOpacity
                  onPress={handleConfirmPasswordVisibility}
                >
                  <Icon
                    name={confirmPasswordIcon}
                    size={28}
                    color='grey'
                  />
                </TouchableOpacity>
              }
            />
            <ErrorMessage
              errorValue={touched.confirmPassword && errors.confirmPassword}
            />

            <ErrorMessage
              errorValue={submitError}
            />

            <View style={styles.buttonContainer}>
              <FormButton
                buttonType='solid'
                onPress={handleSubmit}
                title='Sign Up'
                buttonColor={colors.primary}
                titleColor={colors.white}
                disabled={!isValid}
              />
            </View>

          </>
        )}
      </Formik>
      <Button
        title='Have an account? Login'
        onPress={() => navigation.push('Login')}
        titleStyle={{
          color: colors.primary
        }}
        type='clear'
      />
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  buttonContainer: {
    margin: 25
  }
})

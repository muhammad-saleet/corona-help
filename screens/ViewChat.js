import React, { useContext, useEffect, useState } from 'react'
import { GiftedChat, Bubble } from 'react-native-gifted-chat'
import { AppContext } from '../shared/context'
import { colors } from '../shared/styles'

export const ViewChat = ({ route }) => {
  const { chats, addOutgoingMessage, user } = useContext(AppContext)

  const interlocutorId = route.params.interlocutorUser.id
  const [messages, setMessages] = useState([])

  useEffect(() => {
    const chat = chats.find((c) => c.interlocutorUser.id === interlocutorId)
    setMessages(chat.messages)
  })

  function bubbleRender (props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
            backgroundColor: colors.lightgrey
          }
        }}
      />
    )
  }

  return (
    <GiftedChat
      messages={messages}
      onSend={msg => {
        addOutgoingMessage(msg[0], interlocutorId)
      }}
      user={{
        _id: user.id
      }}
      renderAvatar={null}
      inverted={false}
      renderFooter={() => <></>}
      renderBubble={bubbleRender}
      scrollToBottom
    />
  )
}

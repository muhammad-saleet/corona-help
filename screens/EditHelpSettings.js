import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React, { useContext, useState } from 'react'
import reactotron from 'reactotron-react-native'
import * as Yup from 'yup'
import { Formik } from 'formik'
import FormButton from '../components/FormButton'
import { colors } from '../shared/styles'
import { TagList } from '../components/TagList'
import { LocationView } from '../components/LocationView'
import { AppContext } from '../shared/context'
import { TextInput } from 'react-native-paper'
import ErrorMessage from '../components/ErrorMessage'
import deviceStorage from '../shared/deviceStorage'

export const EditHelpSettings = ({ navigation }) => {
  const { location, setFirstUse } = useContext(AppContext)

  const [tags, setTags] = useState([])
  const [postLocation, setPostLocation] = useState(location)
  const [helpRadius, setHelpRadius] = useState(1000)

  async function storePost (values) {
    setTimeout(() => {
      reactotron.log({ 'submitted values': values })
    }, 1000)
  }

  async function handleSubmit (values) {
    values.tags = tags
    values.postLocation = postLocation

    await storePost(values)
    await deviceStorage.changeFirstUse('false', setFirstUse)
  }

  const validationSchema = Yup.object().shape({
    radius: Yup.number()
      .typeError('Radius must be a number')
      .label('Radius')
      .required('Please specify help radius in meters')
      .min(100, 'Radius must be 100-10000 meters')
      .max(10000, 'Radius must be 100-10000 meters')
  })

  return (
    <ScrollView>

      <LocationView
        isPinDraggable
        location={postLocation}
        setLocationOnPinDrag={setPostLocation}
        title='Your location'
        description='Tap and hold to drag pin'
        radius={helpRadius}
      />

      <Formik
        initialValues={{ radius: '' }}
        onSubmit={values => {
          handleSubmit(values)
        }}
        validationSchema={validationSchema}
      >
        {({
          handleChange,
          values,
          handleSubmit,
          errors,
          isValid,
          handleBlur,
          isSubmitting
        }) => (

          <View style={{ flex: 1 }}>

            <Text style={styles.text}>
              Specify help radius. You will only be notified about requests
              in your help area:
            </Text>

            <TextInput
              style={{ marginHorizontal: 25, marginVertical: 5 }}
              mode='outlined'
              name='radius'
              label='Radius'
              placeholder='1000'
              value={values.radius}
              onChangeText={handleChange('radius')}
              onBlur={() => {
                handleBlur('radius')
                if (!errors.radius) {
                  setHelpRadius(parseInt(values.radius))
                }
              }}
            />

            <ErrorMessage errorValue={errors.radius} />

            <Text style={styles.text}>
              What Can you help with?
            </Text>

            <TagList
              tags={tags}
              setTags={setTags}
              editMode
            />

            <View style={{ margin: 25, marginBottom: 50 }}>
              <FormButton
                title='Save'
                onPress={handleSubmit}
                buttonColor={colors.primary}
                titleColor={colors.white}
                disabled={!isValid || isSubmitting}
                loading={isSubmitting}
              />
            </View>
          </View>
        )}
      </Formik>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  text: {
    marginHorizontal: 30,
    marginVertical: 10,
    fontSize: 18,
    color: colors.black
  }
})

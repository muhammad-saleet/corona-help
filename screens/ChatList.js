import React, { useContext } from 'react'
import { FlatList, View, TouchableNativeFeedback } from 'react-native'
import { AppContext } from '../shared/context'
import { ChatCard } from '../components/ChatCard'
import { colors } from '../shared/styles'

export function ChatList ({ navigation }) {
  const { chats } = useContext(AppContext)
  const interlocutorUser = {
    id: 31,
    name: 'Joseph Saker',
    avatar: 'https://images.unsplash.com/photo-1542345812-d98b5cd6cf98?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjE3Nzg0fQ',
    birthday: '1986-05-26',
    gender: 'Male',
    profession: 'Teacher',
    email: 'joe@gmail.com',
    facebook_id: null,
    google_id: null,
    password: '$2b$10$xhBwWH8hSUySQlaHXX08t.8f9CF3b9ED5JrgES169c.lHKzjb8xsi'
  }
  function renderDivider () {
    return (
      <View
        style={{
          borderBottomColor: colors.lightgrey,
          borderBottomWidth: 1,
          margin: 5
        }}
      />
    )
  }

  function renderChatList ({ item }) {
    const data = {
      name: item.interlocutorUser.name,
      avatar: item.interlocutorUser.avatar,
      lastMsg: item.messages.length ? item.messages[item.messages.length - 1].text : ''
    }

    return (
      <TouchableNativeFeedback style={{ flex: 1 }} onPress={() => navigation.navigate('ViewChat', item)}>
        <View>
          <ChatCard chat={data} />
          {renderDivider()}
        </View>
      </TouchableNativeFeedback>
    )
  }

  return (
    <View>
      <FlatList
        data={chats}
        renderItem={renderChatList}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  )
}

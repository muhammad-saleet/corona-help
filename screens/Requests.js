import { ActivityIndicator, FlatList, RefreshControl, StyleSheet, View } from 'react-native'
import React, { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import { RequestCard } from '../components/RequestCard'
import reactotron from 'reactotron-react-native'
import { AppContext } from '../shared/context'

export const Requests = ({ navigation }) => {
  const { user } = useContext(AppContext)
  // todo: location should be changed dynamically
  const [screenLocation, setScreenLocation] = useState({ longitude: 35.833878, latitude: 34.428670 })
  const [posts, setPosts] = useState([])
  const [lastFetchedId, setLastFetchedId] = useState(0)
  const [refreshing, setRefreshing] = useState(false)

  // sends a help offer
  async function help (postId) {
    const request = { postId, helpingUserId: user.id }

    await axios.post('http://192.168.1.121:4000/posts/help/', request)
    setPosts((oldPosts) => {
      return oldPosts.filter((p) => p.id !== postId)
    })
  }

  // adds a userId-postId pair to ignore table
  async function ignore (postId) {
    const request = { postId, helpingUserId: user.id }

    await axios.post('http://192.168.1.121:4000/posts/ignore/', request)
    setPosts((oldPosts) => {
      return oldPosts.filter((p) => p.id !== postId)
    })
  }

  useEffect(() => {
    let didCancel = false
    const fetchData = async () => {
      if (screenLocation.longitude === undefined) return

      // todo: radius should be taken from help settings
      const requestData = { helpingUserId: user.id, radius: 10000, location: screenLocation, lastFetchedId: lastFetchedId }

      const response = await axios.post(
        'http://192.168.1.121:4000/posts/near_user',
        requestData
      )

      if (response.data && !response.data.length) {
        setLastFetchedId(-1)
        return
      }

      if (!didCancel) {
        setPosts(posts.concat(response.data))
      }
    }
    // only fetch if there is more posts to fetch
    if (lastFetchedId > -1) fetchData()
    return () => {
      didCancel = true
    }
  }, [lastFetchedId])

  function handleLoadMore () {
    // if there are no more posts to fetch return
    if (lastFetchedId === -1) return
    setLastFetchedId(posts[posts.length - 1].id)
  }

  function handleRefresh () {
    setRefreshing(true)
    setPosts([])
    setLastFetchedId(0)
    setRefreshing(false)
  }

  function renderCard ({ item }) {
    return (
      <View style={{ flex: 1 }}>
        <RequestCard
          item={item}
          help={help}
          ignore={ignore}
          navigateToViewRequest={() => { navigation.navigate('ViewRequest', { item }) }}
          navigateToUserProfile={() => navigation.navigate('ViewUserProfile', item.user)}
        />
      </View>
    )
  }

  function renderFooter () {
    // if there are no more posts to fetch, dont render spinner
    if (lastFetchedId === -1) {
      return null
    } else {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size='large' />
        </View>
      )
    }
  }

  return (
    <View>
      <FlatList
        data={posts}
        renderItem={renderCard}
        keyExtractor={(item, index) => index.toString()}
        onEndReached={handleLoadMore}
        onEndReachedThreshold={0.5}
        ListFooterComponent={renderFooter}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={handleRefresh}
          />
        }
      />
    </View>
  )
}

const styles = StyleSheet.create({
  loader: {
    alignItems: 'center',
    marginBottom: 30
  }
})

import { ScrollView, Text, ToastAndroid, View, TouchableOpacity } from 'react-native'
import React, { useContext, useState } from 'react'
import { AppContext } from '../shared/context'
import reactotron from 'reactotron-react-native'
import * as Yup from 'yup'
import { Formik } from 'formik'
import { TextInput } from 'react-native-paper'
import FormButton from '../components/FormButton'
import { colors } from '../shared/styles'
import DateTimePicker from '@react-native-community/datetimepicker'
import { Picker } from '@react-native-community/picker'

// <Button title='Save Edit' onPress={() => navigation.goBack()} />

export const EditUserProfile = ({ navigation }) => {
  const { user, setUser } = useContext(AppContext)
  const [birthday, setBirthday] = useState(new Date())
  const [show, setShow] = useState(false)
  const [gender, setGender] = useState('Prefer not to specify')

  async function saveEdit (values) {
    setTimeout(() => {
      reactotron.log({ 'submitted profile data': values })
    }, 2000)
  }

  function showSubmitted () {
    ToastAndroid.show('Post submitted', ToastAndroid.LONG)
  }

  async function handleSaveChanges (values) {
    values.birthday = birthday.toLocaleDateString()
    values.gender = gender
    await saveEdit(values)
    showSubmitted()
  }

  const handleDatePick = (event, selectedDate) => {
    setShow(false)
    const currentDate = selectedDate || birthday
    setBirthday(currentDate)
  }

  const showDatepicker = () => {
    setShow(true)
  }

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .label('Name')
      .required('You need to provide us with your name!')
      .min(3, 'Name must be 3-30 characters')
      .max(30, 'Name must be 3-30 characters'),
    profession: Yup.string()
      .label('Profession')
      .min(3, 'Name must be 3-30 characters')
      .max(30, 'Name must be 3-30 characters')
  })

  return (
    <ScrollView>
      <Formik
        initialValues={{ name: '', profession: '' }}
        onSubmit={values => {
          handleSaveChanges(values)
        }}
        validationSchema={validationSchema}
      >
        {({
          handleChange,
          values,
          handleSubmit,
          errors,
          isValid,
          touched,
          handleBlur,
          isSubmitting
        }) => (
          <View style={{ flex: 1 }}>
            <TextInput
              style={{ marginHorizontal: 25, marginVertical: 15 }}
              mode='outlined'
              name='name'
              label='Name'
              placeholder='Your name'
              value={values.name}
              onChangeText={handleChange('name')}
              onBlur={handleBlur('name')}
              error={touched.name && errors.name}
            />

            <TouchableOpacity onPress={showDatepicker}>
              <TextInput
                style={{ marginHorizontal: 25, marginVertical: 15 }}
                editable={false}
                mode='outlined'
                name='birthday'
                label='Birthday'
                placeholder='birthday'
                value={birthday.toLocaleDateString()}
              />
            </TouchableOpacity>

            <View>
              {show && (
                <DateTimePicker
                  testID='dateTimePicker'
                  timeZoneOffsetInMinutes={0}
                  value={birthday}
                  mode='date'
                  display='spinner'
                  maximumDate={new Date()}
                  onChange={handleDatePick}
                />
              )}
            </View>
            <TextInput
              style={{ marginHorizontal: 25, marginVertical: 15 }}
              mode='outlined'
              label='Gender'
              value=' '
              render={props =>
                <Picker
                  selectedValue={gender}
                  onValueChange={(itemValue, itemIndex) =>
                    setGender(itemValue)}
                >
                  <Picker.Item label='Male' value='Male' />
                  <Picker.Item label='Female' value='Female' />
                  <Picker.Item label='Prefer not to specify' value='Prefer not to specify' />
                </Picker>}
            />

            <TextInput
              style={{ marginHorizontal: 25, marginVertical: 15 }}
              mode='outlined'
              name='profession'
              label='Profession'
              placeholder='profession'
              value={values.profession}
              onChangeText={handleChange('profession')}
              onBlur={handleBlur('profession')}
              error={touched.profession && errors.profession}
            />

            <View style={{ margin: 25, marginBottom: 50 }}>
              <FormButton
                title='Save'
                onPress={handleSubmit}
                buttonColor={colors.primary}
                titleColor={colors.white}
                disabled={!isValid || isSubmitting}
                loading={isSubmitting}
              />
            </View>
          </View>
        )}
      </Formik>
    </ScrollView>
  )
}

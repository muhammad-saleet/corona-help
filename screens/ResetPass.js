import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Formik } from 'formik'
import * as Yup from 'yup'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { ScreenContainer } from '../components/ScreenContainer'
import { colors } from '../shared/styles'

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .label('Email')
    .email('Enter a valid email')
    .required('Please enter a registered email')
})

export function ResetPass ({ navigation }) {
  const handleSubmit = values => {
    if (values.email.length > 0) {
      setTimeout(() => {
        navigation.navigate('Login')
      }, 2000)
    }
  }

  return (
    <ScreenContainer>
      <Formik
        initialValues={{ email: '' }}
        onSubmit={values => {
          handleSubmit(values)
        }}
        validationSchema={validationSchema}
      >
        {({
          handleChange,
          values,
          handleSubmit,
          errors,
          isValid,
          touched,
          handleBlur,
          isSubmitting
        }) => (
          <>
            <FormInput
              name='email'
              value={values.email}
              onChangeText={handleChange('email')}
              placeholder='Enter email'
              autoCapitalize='none'
              iconName='email'
              iconColor={colors.primary}
              onBlur={handleBlur('email')}
              error={touched.email && errors.email}
            />

            <View style={styles.buttonContainer}>
              <FormButton
                buttonType='solid'
                onPress={handleSubmit}
                title='Send Reset Link'
                buttonColor={colors.primary}
                titleColor={colors.white}
                disabled={!isValid || isSubmitting}
                loading={isSubmitting}
              />
            </View>
          </>
        )}
      </Formik>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  buttonContainer: {
    margin: 25
  }
})

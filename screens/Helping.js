import { FlatList, RefreshControl, View } from 'react-native'
import React, { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import reactotron from 'reactotron-react-native'
import { AppContext } from '../shared/context'
import { HelpingCard } from '../components/HelpingCard'

export const Helping = ({ navigation }) => {
  const { user } = useContext(AppContext)
  const [posts, setPosts] = useState([])
  const [refreshing, setRefreshing] = useState(false)

  // will withdraw help offer
  async function withdraw (postId) {
    const request = { postId, helpingUserId: user.id }

    await axios.post('http://192.168.1.121:4000/posts/withdraw/', request)
    setPosts((oldPosts) => {
      return oldPosts.filter((p) => p.id !== postId)
    })
  }

  // will delete entry form helping table
  async function exit (postId) {
    const request = { postId, helpingUserId: user.id }

    await axios.post('http://192.168.1.121:4000/posts/exit/', request)
    setPosts((oldPosts) => {
      return oldPosts.filter((p) => p.id !== postId)
    })
  }

  // todo: refreshing not working
  useEffect(() => {
    let didCancel = false
    const fetchData = async () => {
      const response = await axios.get(
        'http://192.168.1.121:4000/posts/helping/' + user.id
      )

      if (!didCancel) {
        setPosts(posts.concat(response.data))
      }
    }

    fetchData()
    return () => {
      didCancel = true
    }
  }, [])

  function handleRefresh () {
    setRefreshing(true)
    setPosts([])
    setRefreshing(false)
  }

  function renderCard ({ item }) {
    return (
      <View style={{ flex: 1 }}>
        <HelpingCard
          item={item}
          navigateToViewRequest={() => { navigation.navigate('ViewRequest', { item, UserIsHelping: true }) }}
          navigateToUserProfile={() => navigation.navigate('ViewUserProfile', item.user)}
          withdraw={withdraw}
          exit={exit}
        />
      </View>
    )
  }

  return (
    <View>
      <FlatList
        data={posts}
        renderItem={renderCard}
        keyExtractor={(item, index) => index.toString()}
        onEndReachedThreshold={0.5}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={handleRefresh}
          />
        }
      />
    </View>
  )
}

import { StyleSheet, View } from 'react-native'
import React, { useContext } from 'react'
import { AppContext } from '../shared/context'
import FormButton from '../components/FormButton'
import { colors } from '../shared/styles'
import deviceStorage from '../shared/deviceStorage'

export const RequestOrOffer = ({ navigation }) => {
  const { setFirstUse } = useContext(AppContext)

  return (
    <View style={styles.container}>
      <View style={styles.buttonContainer}>
        <FormButton
          buttonType='solid'
          onPress={() => navigation.navigate('GetLocation', { nextScreen: 'CreateRequest' })}
          title='Request Help'
          buttonColor={colors.primary}
          titleColor={colors.white}
        />
      </View>

      <View style={styles.buttonContainer}>
        <FormButton
          buttonType='solid'
          onPress={() => navigation.navigate('GetLocation', { nextScreen: 'EditHelpSettings' })}
          title='Offer Help'
          buttonColor={colors.primary}
          titleColor={colors.white}
        />
      </View>

      <View style={styles.buttonContainer}>
        <FormButton
          buttonType='solid'
          onPress={() => deviceStorage.changeFirstUse('false', setFirstUse)}
          title='skip'
          buttonColor={colors.lightgrey}
          titleColor={colors.black}
        />
      </View>

    </View>
  )
}

const styles = StyleSheet.create({
  text: {
    margin: 25,
    color: colors.black,
    fontSize: 24
  },
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  buttonContainer: {
    marginHorizontal: 25,
    marginVertical: 15
  }
})

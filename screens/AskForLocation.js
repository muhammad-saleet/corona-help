import {
  Text,
  Button, StyleSheet, View
} from 'react-native'

import React, { useEffect } from 'react'
import { AppContext } from '../shared/context'
import { colors } from '../shared/styles'
import { LocationServices } from '../shared/LocationServices'

export function AskForLocation ({ navigation }) {
  const { setLocation } = React.useContext(AppContext)

  useEffect(() => {
    const callGetLocation = async () => {
      if (await LocationServices.hasLocationPermission()) {
        await LocationServices.getLocation(setLocation)
        navigation.push('RequestOrOffer')
      }
    }

    callGetLocation()
  }, [])

  async function requestPermission () {
    if (await LocationServices.requestPermission()) {
      await LocationServices.getLocation(setLocation)
      navigation.push('RequestOrOffer')
    }
  }

  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        Corona help needs access to your location
        to connect you to people in your area
      </Text>
      <Button
        title='Request Location' onPress={requestPermission}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  text: {
    margin: 25,
    color: colors.black,
    fontSize: 24

  },
  container: {
    flex: 1,
    alignItems: 'center'
  }
})

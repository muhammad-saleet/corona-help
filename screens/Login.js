import React, { useState } from 'react'
import { StyleSheet, View, TouchableOpacity } from 'react-native'
import { Button, SocialIcon, Text, Divider } from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { HideWithKeyboard } from 'react-native-hide-with-keyboard'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import AppLogo from '../components/AppLogo'
import { AppContext } from '../shared/context'
import { ScreenContainer } from '../components/ScreenContainer'
import { colors } from '../shared/styles'
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk'
import axios from 'axios'
import deviceStorage from '../shared/deviceStorage'
import reactotron from 'reactotron-react-native'
import ErrorMessage from '../components/ErrorMessage'
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin'

export function Login ({ navigation }) {
  const { setToken, setUser } = React.useContext(AppContext)
  const [passwordVisibility, setPasswordVisibility] = useState(true)
  const [rightIcon, setRightIcon] = useState('visibility')
  const [connectionError, setConnectionError] = useState(null)

  async function emailLogin (credentials) {
    try {
      const response = await axios.post('http://192.168.1.121:4000/auth/login/email', credentials)
      await deviceStorage.addToken(response.data.token, setToken)
      await deviceStorage.addUser(response.data.user, setUser)
    } catch (error) {
      reactotron.log(error.message)
      if (error.message === 'Request failed with status code 401') {
        setConnectionError('Wrong email or password')
      } else {
        setConnectionError('Server Error: Could not login')
      }
    }
  }

  async function googleLogin (googleUser) {
    try {
      const response = await axios.post('http://192.168.1.121:4000/auth/login/google', googleUser)
      await deviceStorage.addToken(response.data.token, setToken)
      await deviceStorage.addUser(response.data.user, setUser)
    } catch (error) {
      reactotron.log(error.message)
      if (error.message === 'Request failed with status code 401') {
        setConnectionError('Wrong email or password')
      } else {
        setConnectionError('Server Error: Could not login')
      }
    }
  }

  async function facebookLogin (fbUser) {
    try {
      const response = await axios.post('http://192.168.1.121:4000/auth/login/facebook', fbUser)
      await deviceStorage.addToken(response.data.token, setToken)
      await deviceStorage.addUser(response.data.user, setUser)
    } catch (error) {
      reactotron.log(error.message)
      if (error.message === 'Request failed with status code 401') {
        setConnectionError('Wrong email or password')
      } else {
        setConnectionError('Server Error: Could not login')
      }
    }
  }

  async function loginWithGoogle () {
    GoogleSignin.configure()
    try {
      await GoogleSignin.hasPlayServices()
      const userInfo = await GoogleSignin.signIn()
      const googleUser = { id: userInfo.user.id, name: userInfo.user.name, email: userInfo.user.email, photo: userInfo.user.photo }
      await googleLogin(googleUser)
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  }

  const loginWithFacebook = () => {
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function (result) {
        if (result.isCancelled) {
          reactotron.log('Login cancelled')
        } else {
          reactotron.log(
            'Login success with permissions: ' +
            result.grantedPermissions.toString()
          )
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              const accessToken = data.accessToken.toString()
              reactotron.log(accessToken)
              getInfoFromToken(accessToken)
            })
        }
      },
      function (error) {
        reactotron.log('Login fail with error: ' + error)
      }
    )
  }

  function getInfoFromToken (token) {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id,name,picture,email'
      }
    }
    const profileRequest = new GraphRequest(
      '/me',
      { token, parameters: PROFILE_REQUEST_PARAMS },
      (error, fbUser) => {
        if (error) {
          reactotron.log('login info has error: ' + error)
        } else {
          facebookLogin({ ...fbUser, picture: fbUser.picture.data.url })
        }
      }
    )
    new GraphRequestManager().addRequest(profileRequest).start()
  }

  const handlePasswordVisibility = review => {
    setRightIcon(prevState =>
      prevState === 'visibility' ? 'visibility-off' : 'visibility'
    )
    setPasswordVisibility(prevState => !prevState)
  }

  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .label('Email')
      .email('Enter a valid email')
      .required('Please enter a registered email'),
    password: Yup.string()
      .label('Password')
      .required('Please enter your password')
  })

  return (
    <ScreenContainer>
      <HideWithKeyboard style={styles.logoContainer}>
        <AppLogo />
      </HideWithKeyboard>
      <Formik
        initialValues={{ email: '', password: '' }}
        onSubmit={credentials => {
          emailLogin(credentials)
        }}
        validationSchema={validationSchema}
      >
        {({
          handleChange,
          values,
          handleSubmit,
          errors,
          isValid,
          touched,
          handleBlur
        }) => (
          <>
            <FormInput
              name='email'
              value={values.email}
              onChangeText={handleChange('email')}
              placeholder='Email'
              autoCapitalize='none'
              iconName='email'
              iconColor={colors.primary}
              onBlur={handleBlur('email')}
            />

            <ErrorMessage
              errorValue={touched.email && errors.email}
            />

            <FormInput
              name='password'
              value={values.password}
              onChangeText={handleChange('password')}
              placeholder='Password'
              secureTextEntry={passwordVisibility}
              iconName='lock'
              iconColor={colors.primary}
              onBlur={handleBlur('password')}
              rightIcon={
                <TouchableOpacity onPress={handlePasswordVisibility}>
                  <Icon name={rightIcon} size={28} color='grey' />
                </TouchableOpacity>
              }
            />
            <ErrorMessage
              errorValue={touched.password && (errors.password || connectionError)}
            />

            <View style={{ alignSelf: 'flex-start', marginLeft: 20 }}>
              <Button
                title='Forgot password?'
                onPress={() => navigation.push('ResetPass')}
                titleStyle={{
                  color: colors.primary
                }}
                type='clear'
              />
            </View>

            <View style={styles.buttonContainer}>
              <FormButton
                buttonType='solid'
                onPress={handleSubmit}
                title='Login'
                buttonColor={colors.primary}
                titleColor={colors.white}
                disabled={!isValid}
              />
            </View>
          </>
        )}
      </Formik>
      <Button
        title="Don't have an account? Sign Up"
        onPress={() => navigation.push('CreateAccount')}
        titleStyle={{
          color: colors.primary
        }}
        type='clear'
        containerStyle={{
          margin: -10
        }}
      />
      <View style={{
        flexDirection: 'row',
        marginTop: 20,
        marginHorizontal: 45,
        alignItems: 'center',
        justifyContent: 'center'
      }}
      >
        <Divider style={{ flex: 1, backgroundColor: 'black' }} />
        <Text
          style={{
            flex: 1,
            color: colors.black,
            fontSize: 16,
            marginLeft: 10,
            marginRight: 10
          }}
        >Login with
        </Text>
        <Divider style={{ flex: 1, backgroundColor: 'black' }} />
      </View>

      <View style={{ flexDirection: 'row', margin: 15 }}>
        <SocialIcon
          button
          title='Facebook'
          type='facebook'
          style={{ flex: 1, borderRadius: 5, padding: 5 }}
          onPress={() => loginWithFacebook()}
        />

        <SocialIcon
          button
          title='Google'
          type='google'
          style={{ flex: 1, borderRadius: 5, padding: 5 }}
          onPress={() => loginWithGoogle()}
        />
      </View>

    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  logoContainer: {
    marginTop: 50,
    marginBottom: 20,
    alignItems: 'center'
  },
  buttonContainer: {
    margin: 25
  }
})

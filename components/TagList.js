import React, { useState } from 'react'
import { Input } from 'react-native-elements'
import { ToastAndroid, View } from 'react-native'
import Tag from './Tag'
import { colors } from '../shared/styles'
import FormButton from './FormButton'

function TagAddBox ({ addTag }) {
  const [newTag, setNewTag] = useState('')

  const handleChange = val => {
    setNewTag(val)
  }

  return (
    <View style={{
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
      margin: 25,
      height: 45
    }}
    >
      <Input
        name='tagInput'
        placeholderTextColor='grey'
        placeholder='Tag Name'
        inputContainerStyle={{ borderWidth: 1, borderRadius: 8, paddingHorizontal: 5 }}
        containerStyle={{ flex: 1, marginLeft: -10, height: 50 }}
        onChangeText={handleChange}
        value={newTag}
      />
      <FormButton
        title='Add Tag'
        onPress={() => addTag(newTag, setNewTag)}
        buttonColor={colors.secondary}
        titleColor={colors.black}
      />
    </View>
  )
}

export function TagList ({ tags, setTags, editMode }) {
  const maxTagsAllowed = 5

  const addTag = (text, setNewTag) => {
    const isTagAdded = (tagName) => {
      for (const tag of tags) {
        if (tag.text === tagName) return true
      }
      return false
    }

    const validate = (text) => {
      let err = null
      if (tags.length >= maxTagsAllowed) {
        err = 'Posts can have a maximum of 5 tags'
      } else if (text.length < 3 || text.length > 25) {
        err = 'Tags must be 3-25 characters long'
      } else if (isTagAdded(text)) {
        err = 'You already added this tag!'
      }
      return err
    }

    const err = validate(text)
    if (err === null) {
      setNewTag('')
      setTags(prevTags => {
        return [
          ...prevTags,
          { text: text, key: Math.floor(Math.random() * (2 ** 32)).toString() }
        ]
      })
    } else {
      ToastAndroid.show(err, ToastAndroid.LONG)
    }
  }

  const removeTag = (key) => {
    setTags(prevTags => {
      return prevTags.filter(tag => tag.key !== key)
    })
  }

  function tagList () {
    return tags.map((tag) => {
      return (
        <View style={{ margin: 25 }} key={tag.key}>
          {editMode
            ? <Tag
              onPress={() => removeTag(tag.key)}
              title={tag.text}
              removable={true}
            />
            : <Tag
              title={tag.text}
              removable={false}
            />
          }
        </View>
      )
    })
  }

  return (
    <View>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          marginHorizontal: 25,
          justifyContent: 'flex-start',
          alignItems: 'flex-start',
          flexWrap: 'wrap'
        }}
      >
        {tagList()}
      </View>

      {editMode &&
        <TagAddBox addTag={addTag} />}

    </View>
  )
}

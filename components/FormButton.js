import React from 'react'
import { Button } from 'react-native-elements'

const FormButton = ({ buttonColor, titleColor, ...rest }) => (
  <Button
    {...rest}
    type='solid'
    buttonStyle={{ borderRadius: 8, backgroundColor: buttonColor, height: 50 }}
    titleStyle={{ color: titleColor, fontSize: 18 }}
  />
)

export default FormButton

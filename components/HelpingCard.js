import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Avatar } from 'react-native-elements'
import { colors } from '../shared/styles'
import reactotron from 'reactotron-react-native'
import React, { useState } from 'react'
import CardButton from './CardButton'
import { AppContext } from '../shared/context'

export function HelpingCard ({ item, withdraw, exit, navigateToViewRequest, navigateToUserProfile }) {

  // todo: state should be updated live using socket
  // requestState possible values: 'pendingApproval' or 'inProgress'
  const [requestState, setRequestState] = useState('inProgress')

  function renderDivider () {
    return (
      <View
        style={{
          borderBottomColor: colors.lightgrey,
          borderBottomWidth: 1,
          margin: 5
        }}
      />
    )
  }

  function renderPostDetails (item) {
    return (
      <>
        {renderDivider()}
        <Text style={{ margin: 5, fontWeight: 'bold' }}>
          {item.title}
        </Text>
        <Text style={{ margin: 5 }}>
          {(item.description.length <= 100) ? item.description : item.description.slice(0, 100) + '...'}
        </Text>
      </>
    )
  }

  function renderRequestingUser (item) {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={{ margin: 5 }}>
          <Avatar
            rounded
            size='medium'
            icon={{
              name: 'person',
              size: 35,
              color: colors.grey
            }}
            source={{
              uri: item.user.avatar
            }}
            onPress={() => {
              reactotron.log('avatar pressed')
              navigateToUserProfile(item.user)
            }}
            avatarStyle={{ borderWidth: 1, borderRadius: 50, borderColor: colors.secondary }}

          />
        </View>

        <View>
          <TouchableOpacity onPress={() => navigateToUserProfile(item.user)}>
            <Text
              style={{ margin: 5, color: colors.primary }}
            >
              {item.user.name}
            </Text>
          </TouchableOpacity>

          <Text style={{ margin: 5, fontSize: 12, color: colors.black, fontStyle: 'italic' }}>
                Posted on {item.created_at.slice(0, 10)}
          </Text>
        </View>
      </View>
    )
  }

  function renderButtons () {
    if (requestState === 'pendingApproval') {
      return (
        <View style={styles.buttonsRowContainer}>
          <View style={{ margin: 5, flexGrow: 1 }}>
            <CardButton
              buttonType='solid'
              onPress={() => withdraw(item.id)}
              title='Exit'
              buttonColor={colors.primary}
              titleColor={colors.white}
            />
          </View>
          <View style={{ margin: 5, flexGrow: 1 }}>
            <CardButton
              buttonType='solid'
              onPress={() => navigateToViewRequest()}
              title='Details'
              buttonColor={colors.lightgrey}
              titleColor={colors.black}
            />
          </View>
        </View>
      )
    } else {
      return (
        <View style={styles.buttonsRowContainer}>
          <View style={{ margin: 5, flexGrow: 1 }}>
            <CardButton
              buttonType='solid'
              onPress={() => exit(item.id)}
              title='Exit'
              buttonColor={colors.primary}
              titleColor={colors.white}
            />
          </View>
          <View style={{ margin: 5, flexGrow: 1 }}>
            <CardButton
              buttonType='solid'
              onPress={() => navigateToViewRequest()}
              title='Details'
              buttonColor={colors.lightgrey}
              titleColor={colors.black}
            />
          </View>
        </View>
      )
    }
  }

  // #d2f8d2 #d9ecfa
  return (
    <View style={{
      borderWidth: 1,
      borderRadius: 10,
      margin: 25,
      padding: 5,
      backgroundColor: '#d9ecfa'
    }}
    >
      {renderRequestingUser(item)}

      <TouchableOpacity onPress={() => navigateToViewRequest()}>
        {renderPostDetails(item)}
      </TouchableOpacity>

      {renderButtons()}
    </View>
  )
}

const styles = StyleSheet.create({
  buttonsRowContainer: {
    flex: 1,
    flexDirection: 'row',
    marginVertical: 5
  }
})

import React from 'react'
import { colors, sizes } from '../shared/styles'
import { Text, View } from 'react-native'
import FormButton from './FormButton'
import { Overlay } from 'react-native-elements'

export const PopUp = ({
  text,
  leftBtnText,
  rightBtnText,
  leftBtnTextColor,
  rightBtnTextColor,
  leftBtnColor,
  rightBtnColor,
  leftBtnOnPress,
  rightBtnOnPress,
  visibility,
  changeVisibility
}) => {
  return (
    <Overlay
      overlayStyle={{
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 25
      }}
      overlayBackgroundColor={colors.white}
      width={Math.round(sizes.screenWidth * 0.9)}
      height={Math.round(sizes.screenWidth * 0.9 * 0.617)}
      borderRadius={25}
      isVisible={visibility}
      onBackdropPress={() => changeVisibility(false)}
    >
      <>
        <Text style={{ fontSize: 22, color: colors.black }}>
          {text}
        </Text>

        <View style={{
          flex: 1,
          flexDirection: 'row',
          marginTop: 50,
          padding: 25
        }}
        >
          {leftBtnText &&
            <View style={{ width: Math.round(sizes.screenWidth * 0.3), marginHorizontal: 10 }}>
              <FormButton
                title={leftBtnText}
                buttonColor={leftBtnColor}
                titleColor={leftBtnTextColor}
                onPress={leftBtnOnPress}
              />
            </View>}

          {rightBtnText &&
            <View style={{ width: Math.round(sizes.screenWidth * 0.3), marginHorizontal: 10 }}>
              <FormButton
                title={rightBtnText}
                buttonColor={rightBtnColor}
                titleColor={rightBtnTextColor}
                onPress={rightBtnOnPress}
              />
            </View>}
        </View>
      </>
    </Overlay>
  )
}

import React from 'react'
import MapView, { PROVIDER_GOOGLE, Marker, Circle } from 'react-native-maps'
import { View } from 'react-native'
import { sizes } from '../shared/styles'

export function LocationView ({ location, setLocationOnPinDrag, isPinDraggable, title, description, radius }) {
  return (
    <View style={{
      width: sizes.mapWidth,
      height: sizes.mapHeight,
      borderWidth: 1,
      borderColor: 'grey',
      alignSelf: 'center',
      marginVertical: 25
    }}
    >
      <MapView
        style={{ width: '100%', height: '100%' }}
        provider={PROVIDER_GOOGLE}
        region={{ latitudeDelta: 0.0922, longitudeDelta: 0.0421, ...location }}
      >
        <Marker
          draggable={isPinDraggable}
          title={title}
          description={description}
          coordinate={location}
          onDragEnd={(e) => setLocationOnPinDrag(e.nativeEvent.coordinate)}
        />

        <Circle
          center={location}
          radius={radius || 0}
          fillColor='rgba(119, 184, 235, 0.46)'
        />

      </MapView>
    </View>
  )
}

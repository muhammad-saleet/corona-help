import { StyleSheet, Text, View } from 'react-native'
import { Avatar } from 'react-native-elements'
import { colors } from '../shared/styles'
import reactotron from 'reactotron-react-native'
import React from 'react'

export function ChatCard ({ chat }) {
  return (
    <View style={{
      borderTopWidth: 1,
      borderColor: colors.lightgrey,
      padding: 5
    }}
    >
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={{ margin: 5 }}>
          <Avatar
            rounded
            size='medium'
            icon={{
              name: 'person',
              size: 35,
              color: colors.grey
            }}
            source={{
              uri: chat.avatar
            }}
            onPress={() => reactotron.log('avatar pressed')}
            avatarStyle={{ borderWidth: 1, borderRadius: 50, borderColor: colors.secondary }}

          />
        </View>

        <View>
          <Text
            style={{ margin: 5, color: colors.primary }}
          >
            {chat.name}
          </Text>

          <Text style={{ margin: 5 }}>
            {chat.lastMsg}
          </Text>
        </View>
      </View>
    </View>
  )
}


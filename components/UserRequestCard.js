import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Avatar } from 'react-native-elements'
import { colors } from '../shared/styles'
import reactotron from 'reactotron-react-native'
import React, { useEffect, useState } from 'react'
import CardButton from './CardButton'

export function UserRequestCard ({ item, accept, reject, exit, finish, navigateToViewRequest, navigateToUserProfile }) {
  // requestState possible values: 'noHelpOffer' 'pendingApproval' or 'inProgress'
  const [requestState, setRequestState] = useState('noHelpOffer')

  useEffect(() => {
    if (item.beingHelpedBy) {
      setRequestState('inProgress')
    } else if (item.helpOffers && item.helpOffers.length) {
      setRequestState('pendingApproval')
    } else {
      setRequestState('noHelpOffer')
    }
  }, [item])

  function handleAccept (itemId, helpingUserId) {
    // setRequestState('inProgress')
    accept(itemId, helpingUserId)
  }

  function handleReject (itemId, helpingUserId) {
    // setRequestState('noHelpOffer')
    reject(itemId, helpingUserId)
  }

  function handleFinish (itemId, helpingUserId) {
    // setRequestState('noHelpOffer')
    finish(itemId, helpingUserId)
  }

  function handleExit (itemId, helpingUserId) {
    // setRequestState('noHelpOffer')
    exit(itemId, helpingUserId)
  }

  function renderDivider () {
    return (
      <View
        style={{
          borderBottomColor: colors.lightgrey,
          borderBottomWidth: 1,
          margin: 5
        }}
      />
    )
  }

  function renderPostDetails (item) {
    return (
      <>
        <Text style={{ margin: 5, fontWeight: 'bold' }}>
          {item.title}
        </Text>
        <Text style={{ margin: 5 }}>
          {(item.description.length <= 100) ? item.description : item.description.slice(0, 100) + '...'}
        </Text>
      </>
    )
  }

  function renderOfferingUser (item) {
    if (requestState === 'noHelpOffer') {
      return (
        <>
          {renderDivider()}
          <View style={{ alignItems: 'center', marginVertical: 5 }}>
            <Text style={{ color: colors.grey, fontStyle: 'italic' }}>
            No help offers for now.
            </Text>
          </View>
        </>
      )
    } else {
      const offeringUser = item.beingHelpedBy || item.helpOffers[0]
      return (
        <>
          {renderDivider()}
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ margin: 5 }}>
              <Avatar
                rounded
                size='medium'
                icon={{
                  name: 'person',
                  size: 35,
                  color: colors.grey
                }}
                source={{
                  uri: offeringUser.avatar
                }}
                onPress={() => {
                  reactotron.log('avatar pressed')
                  navigateToUserProfile(offeringUser)
                }}
                avatarStyle={{ borderWidth: 1, borderRadius: 50, borderColor: colors.secondary }}

              />
            </View>

            <View>
              <TouchableOpacity onPress={() => navigateToUserProfile(offeringUser)}>
                <Text
                  style={{ margin: 5, color: colors.primary }}
                >
                  {offeringUser.name}
                </Text>
              </TouchableOpacity>

              <Text style={{ margin: 5, fontSize: 12, color: colors.black, fontStyle: 'italic' }}>
                Sent on {item.created_at.slice(0, 10)}
              </Text>
            </View>
          </View>
        </>
      )
    }
  }

  function renderButtons () {
    if (requestState === 'pendingApproval') {
      const offeringUser = item.helpOffers[0]
      return (
        <View style={styles.buttonsRowContainer}>
          <View style={{ margin: 5, flexGrow: 1 }}>
            <CardButton
              buttonType='solid'
              onPress={() => handleAccept(item.id, offeringUser.id)}
              title='Accept'
              buttonColor={colors.primary}
              titleColor={colors.white}
            />
          </View>
          <View style={{ margin: 5, flexGrow: 1 }}>
            <CardButton
              buttonType='solid'
              onPress={() => handleReject(item.id, offeringUser.id)}
              title='Reject'
              buttonColor={colors.lightgrey}
              titleColor={colors.black}
            />
          </View>
        </View>
      )
    } else if (requestState === 'inProgress') {
      return (
        <View style={styles.buttonsRowContainer}>
          <View style={{ margin: 5, flexGrow: 1 }}>
            <CardButton
              buttonType='solid'
              onPress={() => handleFinish(item.id, item.beingHelpedBy.id)}
              title='Finish'
              buttonColor={colors.primary}
              titleColor={colors.white}
            />
          </View>
          <View style={{ margin: 5, flexGrow: 1 }}>
            <CardButton
              buttonType='solid'
              onPress={() => handleExit(item.id, item.beingHelpedBy.id)}
              title='Exit'
              buttonColor={colors.lightgrey}
              titleColor={colors.black}
            />
          </View>
        </View>
      )
    }
  }

  // #d2f8d2 #d9ecfa
  return (
    <View style={{
      borderWidth: 1,
      borderRadius: 10,
      margin: 25,
      padding: 5,
      backgroundColor: '#d9ecfa'
    }}
    >
      <TouchableOpacity onPress={() => navigateToViewRequest()}>
        {renderPostDetails(item)}
      </TouchableOpacity>

      {renderOfferingUser(item)}
      {renderButtons()}
    </View>
  )
}

const styles = StyleSheet.create({
  buttonsRowContainer: {
    flex: 1,
    flexDirection: 'row',
    marginVertical: 5
  }
})

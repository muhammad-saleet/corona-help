import React from 'react'
import { Button } from 'react-native-elements'
import { colors } from '../shared/styles'

const Tag = ({ title, removable, ...rest }) => (
  <Button
    {...rest}
    type='solid'
    title={title}
    buttonStyle={{ borderRadius: 18, backgroundColor: colors.secondary }}
    containerStyle={{ margin: -20 }}
    iconContainerStyle={{ marginHorizontal: 4 }}
    titleStyle={{ color: colors.black, marginHorizontal: 4, fontSize: 14 }}
    iconRight
    icon={
      removable
        ? {
          name: 'cancel',
          size: 18,
          color: colors.black
        }
        : null
    }
  />
)

export default Tag

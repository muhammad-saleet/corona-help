import React from 'react'
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialIcons'

const CardButton = ({ buttonColor, titleColor, disabled, ...rest }) => (
  <Button
    disabled={disabled}
    {...rest}
    type='solid'
    buttonStyle={{ borderRadius: 8, backgroundColor: buttonColor, height: 35, paddingHorizontal: 25 }}
    titleStyle={{ color: titleColor, fontSize: 14 }}
    iconRight
  />
)

export default CardButton

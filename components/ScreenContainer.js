import { ScrollView } from 'react-native'
import React from 'react'

export const ScreenContainer = ({ children }) => (
  <ScrollView style={{ flex: 1 }}>{children}</ScrollView>
)

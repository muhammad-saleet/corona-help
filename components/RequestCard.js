import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Avatar } from 'react-native-elements'
import { colors } from '../shared/styles'
import reactotron from 'reactotron-react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import React, { useContext } from 'react'
import CardButton from './CardButton'
import { AppContext } from '../shared/context'

export function RequestCard ({ item, help, ignore, navigateToViewRequest, navigateToUserProfile }) {
  const { user } = useContext(AppContext)
  const isUserPost = user.id === item.user.id

  // #d2f8d2 #d9ecfa
  return (
    <View style={{
      borderWidth: 1,
      borderRadius: 10,
      margin: 25,
      padding: 5,
      backgroundColor: '#d9ecfa'
    }}
    >
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={{ margin: 5 }}>
          <Avatar
            rounded
            size='medium'
            icon={{
              name: 'person',
              size: 35,
              color: colors.grey
            }}
            source={{
              uri: item.user.avatar
            }}
            onPress={() => {
              reactotron.log('avatar pressed')
              navigateToUserProfile(item.user)
            }}
            avatarStyle={{ borderWidth: 1, borderRadius: 50, borderColor: colors.secondary }}

          />
        </View>

        <View>
          <TouchableOpacity onPress={() => navigateToUserProfile(item.user)}>
            <Text
              style={{ margin: 5, color: colors.primary }}
            >
              {item.user.name}
            </Text>
          </TouchableOpacity>

          <Text style={{ margin: 5, fontSize: 12, color: colors.black, fontStyle: 'italic' }}>
            Posted on {item.created_at.slice(0, 10)}
          </Text>
        </View>

        <Icon
          name='keyboard-arrow-right'
          size={24}
          onPress={() => navigateToViewRequest()}
          style={{ alignSelf: 'center', position: 'absolute', right: 15 }}
        />
      </View>

      <Text style={{ margin: 5, borderTopWidth: 1, borderTopColor: colors.lightgrey, paddingTop: 8, fontWeight: 'bold' }}>
        {item.title}
      </Text>
      <Text style={{ margin: 5 }}>
        {(item.description.length <= 100) ? item.description : item.description.slice(0, 100) + '...'}
      </Text>

      <View style={styles.buttonsRowContainer}>
        <View style={{ margin: 5, flexGrow: 1 }}>
          <CardButton
            disabled={isUserPost}
            buttonType='solid'
            onPress={() => help(item.id)}
            title='Help'
            buttonColor={colors.primary}
            titleColor={colors.white}
          />
        </View>
        <View style={{ margin: 5, flexGrow: 1 }}>
          <CardButton
            disabled={isUserPost}
            buttonType='solid'
            onPress={() => ignore(item.id)}
            title='Ignore'
            buttonColor={colors.lightgrey}
            titleColor={colors.black}
          />
        </View>
        <View style={{ margin: 5, flexGrow: 1 }}>
          <CardButton
            buttonType='solid'
            onPress={() => navigateToViewRequest()}
            title='Details'
            buttonColor={colors.lightgrey}
            titleColor={colors.black}
          />
        </View>
      </View>

    </View>
  )
}

const styles = StyleSheet.create({
  buttonsRowContainer: {
    flex: 1,
    flexDirection: 'row',
    margin: 5,
    borderTopWidth: 1,
    borderTopColor: colors.lightgrey,
    paddingTop: 8,
    fontWeight: 'bold'
  }
})

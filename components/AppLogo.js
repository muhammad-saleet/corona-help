import React from 'react'
import { Image } from 'react-native-elements'

const AppLogo = () => (
  <Image
    source={require('../assets/images/logo1.png')}
    style={{ width: 119, height: 112 }}
  />
)

export default AppLogo
